//-----------------------------------
s = %s; //simbol s
num = s-2; //numarator
den = s^2+3*s+2; //numitor
//-----------------------------------
G1 = syslin('c',num,den); //functia de transfer
//-----------------------------------
t = 0:0.001:10 ; //timp de la 0 la 10 cu pasul 0.01
//-----------------------------------
y = csim('imp',t,G1);
clf(0);xset("window",0);show_window();
plot2d(t , y , 5 );
xgrid(); //grid
xtitle('Raspuns la Impuls pentru functia de transfer','Timp','Amplitudinea'); //titlu , xlabel , ylabel
//-----------------------------------
G3 = 0.5* (squarewave(t,40)+1);
k = csim(G3,t,G1);
l = csim(sin(t),t,G1);
//-----------------------------------
clf(2);xset("window",2);show_window();
plot2d(t , G3 , 2 );
plot2d(t , k , 3 );
xgrid(); //grid
xtitle('Semnal Dreptunghiular si raspunsul in timp','Timp','Amplitudinea'); //titlu , xlabel , ylabel
//-----------------------------------
clf(3);xset("window",3);show_window();
plot2d(t , sin(t) , 6 );
plot2d(t , l , 5 );
xgrid(); //grid
xtitle('Sinus / Raspuns la sinus','Timp','Amplitudinea'); //titlu , xlabel , ylabel
//-----------------------------------
