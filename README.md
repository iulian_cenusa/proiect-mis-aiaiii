# Proiect MIS ( Modelare,Identificare si Simulare )

Simularea si modelarea unui circuit pe baza unei fucntii de transfer data.Proiectul este pentru laboratorul din cadrul materiei MIS ( Modelare,Identificare si Simulare ) de la specializarea Automatica si Informatica Aplicata, anul III, semestrul 2.

#Simularea

Simularea am realizat-o in programul Scilab.

#Modelarea

Modelarea am realizat-o matematic.

#Realizarea circuitului

Schema electrica corespunzatoare fucntiei de transfer am realizat-o cu amplificatoare operationale, in programul TINA si Xcos.

#Implementare fizica

Implementarea fizica consta in realizarea unei placute electronice pe baza schemei electrice din TINA. Pentru amplificatoarele operationale am folosit circuite integrate UC741.
